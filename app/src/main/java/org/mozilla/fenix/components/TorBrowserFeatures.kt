/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

// Copyright (c) 2020, The Tor Project, Inc.

package org.mozilla.fenix.components

import android.os.StrictMode
import android.content.Context
import mozilla.components.concept.engine.webextension.WebExtension
import mozilla.components.concept.engine.webextension.WebExtensionRuntime
import mozilla.components.support.base.log.logger.Logger
import org.mozilla.fenix.ext.components
import org.mozilla.fenix.ext.settings
import org.mozilla.fenix.tor.TorEvents
import java.io.IOException

object TorBrowserFeatures {
    private val logger = Logger("torbrowser-features")
    private const val NOSCRIPT_ID = "{73a6fe31-595d-460b-a920-fcc0f8843232}"

    private fun installNoScript(
        context: Context,
        runtime: WebExtensionRuntime,
        onSuccess: ((WebExtension) -> Unit),
        onError: ((Throwable) -> Unit)
    ) {
        /**
         * Copy the xpi from assets to cacheDir, we do not care if the file is later deleted.
         */
        val xpiName = "$NOSCRIPT_ID.xpi"
        val addonPath = context.cacheDir.resolve(xpiName)
        val policy = StrictMode.getThreadPolicy()
        try {
            context.assets.open("extensions/$xpiName")
                .use { inStream ->
                    // we don't want penaltyDeath() on disk write
                    StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.LAX)

                    addonPath.outputStream().use { outStream ->
                        inStream.copyTo(outStream)
                    }
                }
        } catch (throwable: IOException) {
            onError(throwable)
            return
        } finally {
            StrictMode.setThreadPolicy(policy)
        }

        /**
         * Install with a file:// URI pointing to the temp location where the addon was copied to.
         */
        runtime.installWebExtension(
            id = NOSCRIPT_ID,
            url = addonPath.toURI().toString(),
            onSuccess = { extension ->
                runtime.setAllowedInPrivateBrowsing(
                    extension,
                    true,
                    onSuccess,
                    onError
                )
            },
            onError = { _, throwable -> onError(throwable) })
    }

    private fun installHTTPSEverywhere(
        runtime: WebExtensionRuntime,
        onSuccess: ((WebExtension) -> Unit),
        onError: ((Throwable) -> Unit)
    ) {
        runtime.installWebExtension(
            id = "https-everywhere-eff@eff.org",
            url = "resource://android/assets/extensions/https-everywhere/",
            onSuccess = onSuccess,
            onError = { _, throwable -> onError(throwable) }
        )
    }

    fun install(context: Context, runtime: WebExtensionRuntime) {
        /**
         * Install HTTPS Everywhere as a builtin addon, with a resource://android/ URI.
         * No signatures will be checked/required and there will be no automatic
         * extension updates. It's ok to always try to install, since for builtin extensions it will
         * be checked internally whether it is necessary to install or not: it will only be done
         * if this is the first time or if it's a newer version.
         */
        installHTTPSEverywhere(
            runtime,
            onSuccess = {
                logger.debug("HTTPS Everywhere extension was installed successfully")
            },
            onError = { throwable ->
                logger.error("Could not install HTTPS Everywhere extension", throwable)
            }
        )

        /**
         *  Install NoScript as a user WebExtension if we have not already done so.
         *  AMO signature is checked, but automatic updates still need to be enabled.
         */
        if (!context.settings().noscriptInstalled) {
            installNoScript(
                context,
                runtime,
                onSuccess = {
                    context.settings().noscriptInstalled = true
                    logger.debug("NoScript extension was installed successfully")
                },
                onError = { throwable ->
                    logger.error("Could not install NoScript extension", throwable)
                }
            )
        }

        /**
         *  If we have not done it yet, enable automatic updates for NoScript and force a
         *  one-time immediate update check, in order to upgrade old profiles and ensure we've got
         *  the latest stable AMO version available on first startup.
         *  We will do it as soon as the Tor is connected, to prevent early addonUpdater activation
         *  causing automatic update checks failures (components.addonUpdater being a lazy prop).
         *  The extension, from then on, should behave as if the user had installed it manually.
         */
        if (context.settings().noscriptUpdated == 0) {
            context.components.torController.registerTorListener(object : TorEvents {
                override fun onTorConnected() {
                    context.components.torController.unregisterTorListener(this)
                    // Enable automatic updates
                    context.components.addonUpdater.registerForFutureUpdates(NOSCRIPT_ID)
                    // Force an immediate update check
                    context.components.addonUpdater.update(NOSCRIPT_ID)
                    context.settings().noscriptUpdated = 1
                }

                @SuppressWarnings("EmptyFunctionBlock")
                override fun onTorConnecting() {
                }

                @SuppressWarnings("EmptyFunctionBlock")
                override fun onTorStopped() {
                }

                @SuppressWarnings("EmptyFunctionBlock")
                override fun onTorStatusUpdate(entry: String?, status: String?) {
                }
            })
        }
    }


}
